# Laravel Ready Docker Image
Helper image for create Laravel project Docker image

## Includes
[hirak/prestissimo](https://github.com/hirak/prestissimo) for faster composer install

## Versions
|Version|PHP|Composer|Prestissimo
|---|---|---|---|
|1.0.0|[7.4.2-fpm](https://github.com/docker-library/php/blob/703a3d0a4e4c149bfd62fc3e7b71645f9496b178/7.4/buster/fpm/Dockerfile)|[1.9.2](https://github.com/composer/docker/blob/47c6a2cc73d7c2c5593f72ed62456d997236c71f/1.9/Dockerfile)|0.3.9|

## Examples
- [Laravel Nova](examples/nova) 
- [Laravel with Nginx](examples/with-nginx) 
