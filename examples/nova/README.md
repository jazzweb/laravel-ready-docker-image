# Laravel Nova Dockerfile example

1. Copy Dockerfile to root of your project (near composer.json)
1. Run `docker build --build-arg NOVA_USERNAME=YOUR_LOGIN --build-arg NOVA_PASSWORD=YOUR_PASSWORD .`

> Notice: better to use [API Token](https://nova.laravel.com/settings#password) instead password
