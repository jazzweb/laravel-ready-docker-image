FROM registry.gitlab.com/jazzweb/laravel-ready-docker-image:1.0.0

ARG LARAVEL_ROOT_PATH="/var/www/laravel"

# Working directory
WORKDIR $LARAVEL_ROOT_PATH

# Use env to path it in entrypoint
ENV LARAVEL_STORAGE_PATH=$LARAVEL_ROOT_PATH/storage

# Create volume for storage
VOLUME $LARAVEL_ROOT_PATH/storage

# Copy all files to working directory
COPY . $LARAVEL_ROOT_PATH

# Give permissions to files
RUN chown -R www-data:www-data $LARAVEL_ROOT_PATH

# Install composer dependencies
RUN composer install --no-dev

# Install nginx
RUN apt-get update && apt-get install -y nginx && rm -rf /var/lib/apt/lists/*

# Copy nginx config
COPY deploy/nginx/default.conf /etc/nginx/sites-available/default

# Set arguments to default conf
RUN sed -i "s#LARAVEL_ROOT_PATH#$LARAVEL_ROOT_PATH#g;" /etc/nginx/sites-available/default

# Set of env variables. Use this instead .env
ENV APP_NAME=Laravel
ENV APP_ENV=production
ENV APP_DEBUG=false
ENV APP_URL=http://localhost

# Add start script
ADD deploy/start.sh /
RUN chmod +x /start.sh

CMD ["/start.sh", "$LARAVEL_STORAGE_PATH"]
