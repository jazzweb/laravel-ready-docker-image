#!/bin/sh

# Clear config
php artisan config:clear

# Reset cache
php artisan cache:clear

# Clear public storage https://stackoverflow.com/a/47902653
rm public/storage

# Create storage symlink
php artisan storage:link

# Add permissions to storage
chown -R www-data:www-data $LARAVEL_STORAGE_PATH

# Add application key to env variables
export APP_KEY=$(php artisan key:generate --show)

# Run migration
php artisan migrate --force

# Start nginx
service nginx start

# Run php-fpm process
php-fpm
